# pilot_electron_reconstruction

Repo for electron clustering and energy reconstruction (FASERnu pilot data analysis). Clone the repo:

git clone https://gitlab.cern.ch/jwspence/pilot_electron_reconstruction.git

Compile FEDRA:

source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/5.34.36/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh

cd pilot_electron_reconstruction/Fedra;./install.sh

Compile and run mktree (for converting FEDRA format to ROOT TTree):

./compile.sh
